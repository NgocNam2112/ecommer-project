import Header from "./components/Header/Header";
import "./App.css";
import HomePage from "./Screen/HomePage/HomePage";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import FruitAndVegetable from "./Screen/FuitAndVegetable/FruitAndVegetable";
import DetailsProduct from "./Screen/DetailsProduct/DetailProduct";
import Test from "./Test";
import CheckoutPage from "./Screen/CheckoutPage/CheckoutPage";
import Blog from "./Screen/Blog/Blog";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { getProducts } from "./actions/productAction";
import SignUP from "./Screen/SignUp/SignUp";
import NotFound from "./Screen/NotFound/NotFound";
import SearchProduct from "./Screen/SearchProduct/SearchProduct";
import { IPayloadProduct } from "./api/types";

const App = () => {
  const dispatch = useDispatch();
  const [searchProduct, setSearchProduct] = useState<Array<IPayloadProduct>>(
    []
  );
  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);

  const getSearchProduct = (item: Array<IPayloadProduct>) => {
    setSearchProduct(item);
  };

  return (
    <div>
      <BrowserRouter>
        <Header getSearchProduct={getSearchProduct} />
        <Switch>
          <Route
            path="/list-product/:catagoryName/:_id"
            exact
            component={FruitAndVegetable}
          />
          <Route path="/test" exact component={Test} />
          <Route path="/detailsProduct/:id" exact component={DetailsProduct} />
          <Route path="/checkout" exact component={CheckoutPage} />
          <Route path="/blogPost" exact component={Blog} />
          <Route path="/signUp" exact component={SignUP} />
          <Route path="/not_found" exact component={NotFound} />
          <Route path="/search-product" exact>
            <SearchProduct searchProduct={searchProduct} />
          </Route>
          {/* <Route
            exact
            path="search-product"
            render={() => <SearchProduct searchProduct={searchProduct} />}
          /> */}
          <Route path="/">
            <HomePage />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default App;
