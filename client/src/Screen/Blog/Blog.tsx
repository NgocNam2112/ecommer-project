import { Grid } from "@material-ui/core";
import { useState, useEffect } from "react";
import PageTitle from "../../components/PageTitle/PageTitle";
import GridView from "../../images/FruitAndVegetable/GridView";
import ListView from "../../images/FruitAndVegetable/ListView";
import useStyle from "./styles";
import SpaceForBlog from "../../components/SpaceForBlog/SpaceForBlog";
import BlogImage from "../../images/HomPage/Blog/BlogImage.png";
import AvatarBlog from "../../images/HomPage/Avatar/Ellipse_3.png";
import AdormentInput from "../../components/AdormentInput/AdormentInput";
import Subscribe from "../../images/Checkout/Subscribe";
import BlogPostItem from "../../components/SpaceForBlog/BlogPostItem/BlogPostItem";
import Vegetable from "../../images/HomPage/Blog/Vegetable.png";
import DownWhiteArrow from "../../images/BodyFruit/DownWhiteArrow";
import Footer from "../../components/Footer/Footer";

const Blog = () => {
  const classes = useStyle();
  const [gridView, setGridView] = useState<boolean>(false);

  const blogList = [
    {
      h1: "Archives",
      list: [
        "March 2020",
        "February 2020",
        "January 2020",
        "November 2019",
        "December 2019",
      ],
    },
    {
      h1: "Category",
      list: [
        "Food",
        "Chefs specialities",
        "Vegetable",
        "Meat",
        "Recommendations",
      ],
    },
  ];

  const handleChangeListView = () => {
    setGridView(true);
  };

  const renderBlogTitle = () => {
    return (
      <Grid container xs={12} className={classes["page-title"]}>
        <h1>Fruit and vegetables</h1>
        <div className={classes["page-sort"]}>
          <div onClick={handleChangeListView}>
            <GridView />
            Grid View
          </div>
          <div onClick={handleChangeListView}>
            <ListView />
            List View
          </div>
          <div>
            <p>117</p>
            <p>Products</p>
          </div>
        </div>
      </Grid>
    );
  };

  const renderMainBlog = () => {
    return (
      <>
        <PageTitle page={["Home", "Fruit and vegetables", "Product title"]} />
        <Grid container xs={12} style={{ justifyContent: "space-between" }}>
          <SpaceForBlog
            mainblog={BlogImage}
            tags="tags"
            blogheadline="Our chef tips for a great and tasty dinner ready in 20 minutes"
            avatarauthor={AvatarBlog}
            date="17. 6. 2020"
          />
          <SpaceForBlog
            mainblog={BlogImage}
            tags="tags"
            blogheadline="Our chef tips for a great and tasty dinner ready in 20 minutes"
            avatarauthor={AvatarBlog}
            date="17. 6. 2020"
          />
        </Grid>
      </>
    );
  };

  const renderBlogListCategory = () => {
    return (
      <Grid item xs={2} className={classes["list-blog-category"]}>
        {blogList.map((item) => {
          return (
            <div>
              <h1>{item.h1}</h1>
              <ul>
                {item.list.map((i) => {
                  return <li>{i}</li>;
                })}
              </ul>
            </div>
          );
        })}
        <div className={classes["subscribe-to-read-blog"]}>
          <h1>Join our list</h1>
          <p>
            Signup to be the first to hear about exclusive deals, special
            offers, recepies from our masters and others.
          </p>
          <AdormentInput
            placeholder="Your email address"
            element={<Subscribe />}
          />
        </div>
      </Grid>
    );
  };

  const renderBlogListItem = () => {
    return (
      <Grid item xs={9} className={classes["wrapper-blog-list-item"]}>
        <div>
          <BlogPostItem
            blogitemimage={Vegetable}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={Vegetable}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={Vegetable}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={Vegetable}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={Vegetable}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={Vegetable}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={Vegetable}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={Vegetable}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={Vegetable}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={Vegetable}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={Vegetable}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
      </Grid>
    );
  };

  const renderListBlog = () => {
    return (
      <Grid
        container
        xs={12}
        style={{ display: "flex", justifyContent: "space-between" }}
      >
        {renderBlogListCategory()}
        {renderBlogListItem()}
      </Grid>
    );
  };

  const renderMoreProduct = () => {
    return (
      <Grid item xs={12} className={classes["pagnition-page"]}>
        <div>
          <p>Page</p>
          <p>1</p>
          <p>2</p>
          <p>3</p>
          <p>4</p>
        </div>
        <button>
          Show more products <DownWhiteArrow />
        </button>
        <div>
          <span>336</span>
          <span>Products</span>
        </div>
      </Grid>
    );
  };

  return (
    <div className={classes["root"]}>
      {renderBlogTitle()}
      {renderMainBlog()}
      {renderListBlog()}
      {renderMoreProduct()}
      <Footer />
    </div>
  );
};

export default Blog;
