import { Checkbox, FormControlLabel, FormGroup, Grid } from "@material-ui/core";
import { useState } from "react";
import FedEX from "../../images/Checkout/FedEX";
import PageTitle from "../../components/PageTitle/PageTitle";
import Bitcoin from "../../images/Checkout/Bitcoin";
import CartProductImage from "../../images/Checkout/CartProductImage";
import CheckOutIcon from "../../images/Checkout/CheckOutIcon";
import CloseSign from "../../images/Checkout/CloseSign";
import CompareCheckout from "../../images/Checkout/CompareCheckout";
import DHL from "../../images/Checkout/DHL";
import FavouriteCheckout from "../../images/Checkout/FavouriteCheckout";
import Paypal from "../../images/Checkout/Paypal";
import SecuritySafely from "../../images/Checkout/SecuritySafely";
import UnCheckoutIcon from "../../images/Checkout/UnCheckoutIcon";
import Visa from "../../images/Checkout/Visa";
import CheckboxIcon from "../../images/FruitAndVegetable/CheckboxIcon/CheckboxIcon";
import UnCheckboxIcon from "../../images/FruitAndVegetable/CheckboxIcon/UnCheckboxIcon";
import useStyles from "./styles";
import RingStar from "../../images/BodyFruit/RingStar";
import Stars from "../../images/BodyFruit/Stars";
import DownArrowBlack from "../../images/DownArrowBlack";
import ApplyNow from "../../images/Checkout/ApplyNow";
import AdormentInput from "../../components/AdormentInput/AdormentInput";
import DrawerMenu from "../../components/DrawerMenu/DrawerMenu";

const CheckoutPage = () => {
  const classes = useStyles();

  //state used to check user's new address
  const [checkbox, setCheckbox] = useState<boolean>(false);
  const [fedEx, setFedEx] = useState<boolean>(true);
  const [dhl, setDHL] = useState<boolean>(true);
  const [creditCard, setCreditCard] = useState<boolean>(true);
  const [paypal, setPaypal] = useState<boolean>(true);
  const [bitcoin, setBitcoin] = useState<boolean>(false);
  const [agreeNoSpam, setAgreeNoSpam] = useState<boolean>(true);
  const [agreePolicy, setAgreePolicy] = useState<boolean>(true);

  const renderInput = (name: string, placeholder: string, value?: string) => {
    return (
      <div>
        <h2>{name}</h2>
        <input type="text" placeholder={placeholder} />
      </div>
    );
  };
  const handleChange = (state: boolean, func: Function) => {
    func(!state);
  };
  const formControlLable = (
    icon: JSX.Element,
    checkIcon: JSX.Element,
    text: String,
    state: boolean,
    func: Function
  ) => {
    return (
      <FormGroup row>
        <FormControlLabel
          control={
            <Checkbox
              icon={icon}
              checkedIcon={checkIcon}
              checked={state}
              onChange={() => handleChange(state, func)}
            />
          }
          label={text}
        />
      </FormGroup>
    );
  };

  const renderWrapperInput = () => {
    return (
      <>
        <div className={classes["wrapper-input"]}>
          {renderInput("First name", "First Name")}
          {renderInput("Last name", "Last Name")}
        </div>
        <div className={classes["wrapper-input"]}>
          {renderInput("Email address", "Email")}
          {renderInput("Phone number", "Phone number")}
        </div>
        <div className={classes["wrapper-input"]}>
          {renderInput("Address", "Adress")}
          {renderInput("Town / City", "Town or city")}
        </div>
        <div className={classes["wrapper-input"]}>
          {renderInput("State / Country", "Choose a state or Country")}
          {renderInput("ZIP/Postal code", "Postal code or ZIP")}
        </div>
        <div className={classes["different-address"]}>
          {formControlLable(
            <UnCheckboxIcon />,
            <CheckboxIcon />,
            "Ship to a different address?",
            checkbox,
            setCheckbox
          )}
        </div>
      </>
    );
  };

  const renderAdditionalPrice = (price: string, method: string) => {
    return (
      <div className={classes["additional-price"]}>
        <span>{price}</span>
        <span>{method}</span>
      </div>
    );
  };

  const renderMethodOption = (
    text: string,
    state: boolean,
    func: Function,
    price: string,
    method: string,
    tradeName: JSX.Element
  ) => {
    return (
      <div className={classes["wrapper-additional-price"]}>
        <div className={classes["wrapper-icon-payment"]}>
          {formControlLable(
            <UnCheckoutIcon />,
            <CheckOutIcon />,
            text,
            state,
            func
          )}
          {renderAdditionalPrice(price, method)}
        </div>
        {tradeName}
      </div>
    );
  };

  const titleBlock = (h2: string, method: string, step: string) => {
    return (
      <div>
        <h2 className={classes["title-block-h2"]}>{h2}</h2>
        <div className={classes["title-block-div"]}>
          <p>{method}</p>
          <p>{step}</p>
        </div>
      </div>
    );
  };

  const renderStepTwo = () => {
    return (
      <div className={classes["step-two"]}>
        {titleBlock(
          "Billing method",
          "Please enter your payment method",
          "Step 2 of 5"
        )}
        {renderMethodOption(
          "FedEx",
          fedEx,
          setFedEx,
          "+32 USD",
          "Additional price",
          <FedEX />
        )}
        {renderMethodOption(
          "DHL",
          dhl,
          setDHL,
          "+15 USD",
          "Additional price",
          <DHL />
        )}
      </div>
    );
  };
  const renderInutor = (text: string, placeholder: string) => {
    return (
      <>
        <h3>{text}</h3>
        <input type="text" placeholder={placeholder} />
      </>
    );
  };

  const renderStepThree = () => {
    return (
      <div className={classes["third-step"]}>
        {titleBlock(
          "Payment method",
          "Please enter your payment method",
          "Step 3 of 5"
        )}
        <div>
          <div className={classes["credit-block"]}>
            <div>
              {formControlLable(
                <UnCheckoutIcon />,
                <CheckOutIcon />,
                "Credit Card",
                creditCard,
                setCreditCard
              )}
              <Visa />
            </div>
            <div className={classes["card-number"]}>
              {renderInutor("Card number", "CardNumber")}
            </div>
            <div className={classes["card-holder"]}>
              <div>{renderInutor("Card holder", "Card holder")}</div>
              <div> {renderInutor("Expiration date", "DD/MM/YY")}</div>
              <div>{renderInutor("CVC", "CVC")}</div>
            </div>
          </div>
        </div>
        {renderMethodOption("PayPal", paypal, setPaypal, "", "", <Paypal />)}
        {renderMethodOption(
          "Bitcoin",
          bitcoin,
          setBitcoin,
          "",
          "",
          <Bitcoin />
        )}
      </div>
    );
  };

  const renderFouthStep = () => {
    return (
      <div className={classes["step-four"]}>
        {titleBlock(
          "Additional informations",
          "Need something else? We will make it for you!",
          "Step 4 of 5"
        )}
        <div className={classes["order-note"]}>
          <h2>Order notes</h2>
          <textarea placeholder="Need a specific delivery day? Sending a gitf? Let’s say ..."></textarea>
        </div>
      </div>
    );
  };

  const renderRules = (
    icon: JSX.Element,
    checkIcon: JSX.Element,
    text: String,
    state: boolean,
    func: Function
  ) => {
    return (
      <div className={classes["block-policy"]}>
        {formControlLable(icon, checkIcon, text, state, func)}
      </div>
    );
  };
  const renderFifthstep = () => {
    return (
      <>
        <div className={classes["fifth-step"]}>
          {titleBlock(
            "Confirmation",
            "We are getting to the end. Just few clicks and your order si ready!",
            "Step 5 of 5"
          )}
          <div className={classes["agree-policy"]}>
            {renderRules(
              <CheckboxIcon />,
              <UnCheckboxIcon />,
              "I agree with sending an Marketing and newsletter emails. No spam, promissed!",
              agreeNoSpam,
              setAgreeNoSpam
            )}
            {renderRules(
              <CheckboxIcon />,
              <UnCheckboxIcon />,
              "I agree with our terms and conditions and privacy policy.",
              agreePolicy,
              setAgreePolicy
            )}
          </div>
          <button>Complete order</button>
        </div>
        <div className={classes["secrity-safely"]}>
          <SecuritySafely />
          <div>
            <h2>All your data are safe</h2>
            <p>
              We are using the most advanced security to provide you the best
              experience ever.
            </p>
          </div>
        </div>
        <div className={classes["footer"]}>Copyright © 2020 Freshnesecom</div>
      </>
    );
  };

  const renderStar = (stars: number, ringStars: number) => {
    let arr = [];
    for (let i = 0; i < stars; i++) {
      if (i < ringStars) arr.push(<RingStar />);
      else arr.push(<Stars />);
    }
    return arr;
  };

  const renderCardProduct = () => {
    return (
      <div className={classes["wrapper-product-cart"]}>
        <div className={classes["image"]}>
          <CartProductImage />
          <div>
            <FavouriteCheckout />
            <span>Wishlist</span>
          </div>
          <div>
            <CompareCheckout />
            <span>Compare</span>
          </div>
          <div>
            <CloseSign />
            <span>Remove</span>
          </div>
        </div>
        <div className={classes["product-cart-info"]}>
          <h2>Product title</h2>
          <div className={classes["product-list"]}>
            <span>Farm:</span>
            <span>Freshness:</span>
          </div>
          <div className={classes["product-details"]}>
            <span>Tharamis Farm</span>
            <span>1 day old</span>
          </div>
          {renderStar(5, 4)}
          <div className={classes["product-wrapper"]}>
            <div className={classes["price-product-cart"]}>
              <span>36.99 USD</span>
              <span>48.56 USD</span>
            </div>
            <div className={classes["pcs"]}>
              <span>1</span>
              <span>
                Pcs
                <DownArrowBlack />
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  };

  // contain: Subtotal,Tax, Shipping
  const renderRemainCart = () => {
    return (
      <>
        <div className={classes["total-price"]}>
          <span>Subtotal</span>
          <span>73.98 USD</span>
        </div>
        <div className={classes["total-price"]}>
          <span>Tax</span>
          <span>17% 16.53 USD</span>
        </div>
        <div className={classes["total-price"]}>
          <span>Shipping</span>
          <span>0 USD</span>
        </div>
        <AdormentInput placeholder="Apply promo code" element={<ApplyNow />} />
        <div className={classes["wrapper-cart-footer"]}>
          <div className={classes["total-order"]}>
            <h2>Total Order</h2>
            <p>Guaranteed delivery day: June 12, 2020</p>
          </div>
          <div className={classes["final-price"]}>89.84 USD</div>
        </div>
      </>
    );
  };

  const renderCart = () => {
    return (
      <div className={classes["cotainer-cart-item"]}>
        <div className={classes["cart-title"]}>
          <h2>Order Summary</h2>
          <p>
            Price can change depending on shipping method and taxes of your
            state.
          </p>
        </div>
        {renderCardProduct()}
        {renderCardProduct()}
        {renderRemainCart()}
      </div>
    );
  };
  const renderButtonResponseCart = () => {
    return <>Shopping cart here!</>;
  };
  return (
    <div className={classes["root"]}>
      <PageTitle page={["HomePage", "Checkout page"]} />
      <Grid
        container
        xs={12}
        style={{ display: "flex", justifyContent: "space-between" }}
      >
        <Grid item xl={7} lg={7} md={12} xs={12}>
          <div className={classes["first-step"]}>
            <h1>Billing info</h1>
            <div>
              <p>Please enter your billing info</p>
              <p>Step 1 of 5</p>
            </div>
          </div>
          {renderWrapperInput()}
          {renderStepTwo()}
          {renderStepThree()}
          {renderFouthStep()}
          {renderFifthstep()}
        </Grid>
        <Grid item xl={4} lg={4} md={12} xs={12} className={classes["cart"]}>
          {renderCart()}
        </Grid>
        <Grid item xs={12} className={classes["cart-response"]}>
          <DrawerMenu
            drawerType="bottom"
            blockContent={renderButtonResponseCart}
            content={renderCart}
          />
        </Grid>
      </Grid>
    </div>
  );
};

export default CheckoutPage;
