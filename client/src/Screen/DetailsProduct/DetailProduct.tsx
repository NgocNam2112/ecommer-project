import { Grid } from "@material-ui/core";
import { useEffect, useState } from "react";
import RingStar from "../../images/BodyFruit/RingStar";
import Stars from "../../images/BodyFruit/Stars";
import useStyles from "./styles";
import DownArrowBlack from "../../images/DownArrowBlack";
import AddProductIcon from "../../images/AddProductIcon";
import FavouriteIcon from "../../images/DetailsProduct/FavouriteIcon";
import CompareIcon from "../../images/DetailsProduct/CompareIcon";
import LeftArrowGreen from "../../images/HomPage/LeftArrowGreen";
import ProductItem from "../../components/ProductItem/ProductItem";
import Footer from "../../components/Footer/Footer";
import PageTitle from "../../components/PageTitle/PageTitle";
import { useSelector } from "react-redux";
import { useParams } from "react-router";
import { IPayloadProduct } from "../../api/types";

interface IDetailsProduct {
  title: String;
  details: String[];
}
interface IProduct {
  productTitle: string;
  stars: number;
  detailsIngredients: {
    method: Array<string>;
    state: Array<string>;
  };
  detailDiscription: {
    origin: Array<string>;
    how_to_cook?: Array<string>;
    vitamins?: Array<string>;
  };
  price: number;
  discount: number;
  relatedImage: {
    img1: string;
    img2: string;
  };
  image: string;
  introduceProduct: string;
}

const DetailProduct = () => {
  const { id } = useParams<{ id: string }>();
  const [product, setProduct] = useState<IProduct>();
  const classes = useStyles();
  const [selectOption, setSelectOption] = useState<String>("des");
  const products = useSelector((state: any) => state.product);

  const sellProduct = {
    method: ["SKU:", "Category:", "Stock:", "Farm"],
    state: ["Freshness:", "Buy by:", "Delivery:", "Delivery area"],
  };
  const renderProductImage = () => {
    return (
      <>
        <Grid
          item
          xs={12}
          lg={6}
          md={12}
          className={classes["wrapper-details-page"]}
        >
          <div className={classes["custom-image-product"]}>
            <img src={product?.image} alt="" />
            <div>
              <span>{product?.discount === 0 ? null : `${-36}%`}</span>
              <span>Free shipping</span>
            </div>
          </div>

          <div>
            <img src={product?.relatedImage?.img1} alt="" />
            <img src={product?.relatedImage?.img2} alt="" />
          </div>
          {/* <ThumbImageProduct /> */}
        </Grid>
      </>
    );
  };

  const renderStar = (stars: number, ringStars: number | undefined) => {
    let arr = [];
    for (let i = 0; i < stars; i++) {
      if (i < (ringStars as number)) arr.push(<RingStar />);
      else arr.push(<Stars />);
    }
    return arr;
  };

  const methodAndFormSale = (option: any, productData: any) => {
    return (
      <div>
        <div>
          {option.map((item: string) => {
            return <span>{item}</span>;
          })}
        </div>
        <div>
          {productData
            ? productData.map((item: string) => {
                return <span>{item}</span>;
              })
            : null}
        </div>
      </div>
    );
  };

  const renderProductRelated = () => {
    return (
      <div>
        <h1>{product?.productTitle}</h1>
        <div className={classes["customer-review"]}>
          {renderStar(5, product?.stars)}
          <p>(1 customer review)</p>
        </div>
        <p className={classes["custom-introduce-poduct"]}>
          {product?.introduceProduct}
        </p>
        <div className={classes["related-product"]}>
          {methodAndFormSale(
            sellProduct.method,
            product?.detailsIngredients?.method
          )}
          {methodAndFormSale(
            sellProduct.state,
            product?.detailsIngredients?.state
          )}
        </div>
      </div>
    );
  };
  const discountPrice = () => {
    return (
      ((100 - (product?.discount as number)) / 100) * (product?.price as number)
    );
  };
  const renderPrice = () => {
    return (
      <>
        <div className={classes["product-price"]}>
          <div>
            <h2>{discountPrice()} USD</h2>
            <p>{product?.discount === 0 ? null : `${product?.price} USD`} </p>
          </div>
          <div className={classes["pcs"]}>
            <span>1</span>
            <span>
              Pcs
              <DownArrowBlack />
            </span>
          </div>
          <button>
            <AddProductIcon />
            Add to cart
          </button>
        </div>

        <div className={classes["cutom-button-product"]}>
          <button>
            <FavouriteIcon />
            Add to my wish list
          </button>

          <button>
            <CompareIcon />
            Compare
          </button>
        </div>
      </>
    );
  };

  const nutritionProduction = (option: IDetailsProduct) => {
    return (
      <div className={classes["custom-details-product"]}>
        <h3>{option.title}</h3>
        <ul>
          {option.details.map((item) => {
            return <li>{item}</li>;
          })}
        </ul>
      </div>
    );
  };

  // func set state bang gia tri string nhan vao
  const funcChangeValue = (setState: Function, value: String) => {
    setState(value);
  };

  const renderProductReview = () => {
    return (
      <div className={classes["select-option-related-product"]}>
        <ul>
          <li
            onClick={() => funcChangeValue(setSelectOption, "des")}
            style={
              selectOption === "des"
                ? { borderBottom: "1px solid #6A983C" }
                : { borderBottom: "1px solid #F5F5F5" }
            }
          >
            Description
          </li>
          <li
            onClick={() => funcChangeValue(setSelectOption, "rev")}
            style={
              selectOption === "rev"
                ? { borderBottom: "1px solid #6A983C" }
                : { borderBottom: "1px solid #F5F5F5" }
            }
          >
            Reviews <span>18</span>
          </li>
          <li
            onClick={() => funcChangeValue(setSelectOption, "ques")}
            style={
              selectOption === "ques"
                ? { borderBottom: "1px solid #6A983C" }
                : { borderBottom: "1px solid #F5F5F5" }
            }
          >
            Questions <span>4</span>
          </li>
        </ul>
        {selectOption === "des" ? (
          <div className={classes["product-details"]}>
            <h3>Origins</h3>
            {product?.detailDiscription.origin.map((item: string) => {
              return <p>{item}</p>;
            })}
            <h3>How to cook</h3>
            {product?.detailDiscription?.how_to_cook !== undefined
              ? product?.detailDiscription?.how_to_cook.map((item: string) => {
                  return <p>{item}</p>;
                })
              : null}
            <h3>Full of Vitamins!</h3>
            <div className={classes["wrapper-details-product"]}>
              {product?.detailDiscription?.vitamins !== undefined
                ? product?.detailDiscription?.vitamins.map((item: any) => {
                    return nutritionProduction(item);
                  })
                : null}
            </div>
          </div>
        ) : selectOption === "rev" ? null : null}
      </div>
    );
  };

  const renderProductInfo = () => {
    return (
      <Grid
        item
        xs={12}
        lg={6}
        md={12}
        className={classes["wrapper-product-infor"]}
      >
        {renderProductRelated()}
        {renderPrice()}
        {renderProductReview()}
      </Grid>
    );
  };

  const renderProductDetails = () => {
    return (
      <>
        <PageTitle
          page={["Home", "Fruit and vegetables", "Carrots from Tomissy Farm"]}
        />
        <Grid container xs={12} className={classes["container-product"]}>
          {renderProductImage()}
          {renderProductInfo()}
        </Grid>
        <Grid item xs={12}>
          <div className={classes["title-slide"]}>
            <h2>Related products</h2>
            <button>
              More products
              <LeftArrowGreen />
            </button>
          </div>
          {/* <div className={classes["list-item"]}>
              <ProductItem
                titleProduct="Product Title"
                description="Space for a small product description "
                price={148}
                discount={36}
              />
              <ProductItem
                titleProduct="Product Title"
                description="Space for a small product description "
                price={148}
                discount={36}
              />
              <ProductItem
                titleProduct="Product Title"
                description="Space for a small product description "
                price={148}
                discount={36}
              />
              <ProductItem
                titleProduct="Product Title"
                description="Space for a small product description "
                price={148}
                discount={36}
              />
              <ProductItem
                titleProduct="Product Title"
                description="Space for a small product description "
                price={148}
                discount={36}
              />
            </div> */}
        </Grid>
      </>
    );
  };
  useEffect(() => {
    setProduct(products.find((item: IPayloadProduct) => item._id === id));
  }, [products, id]);

  return (
    <div className={classes.root}>
      {renderProductDetails()}
      <Footer />
    </div>
  );
};

export default DetailProduct;
