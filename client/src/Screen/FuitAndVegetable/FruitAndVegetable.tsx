import React, { useEffect, useState } from "react";
import { Grid } from "@material-ui/core";
import useStyles from "./styles";
import GridView from "../../images/FruitAndVegetable/GridView";
import ListView from "../../images/FruitAndVegetable/ListView";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import CheckIcon from "../../images/FruitAndVegetable/RadioButton/CheckIcon";
import UncheckIcon from "../../images/FruitAndVegetable/RadioButton/UncheckIcon";
import FormGroup from "@material-ui/core/FormGroup";
import Checkbox from "@material-ui/core/Checkbox";
import CheckboxIcon from "../../images/FruitAndVegetable/CheckboxIcon/CheckboxIcon";
import UnCheckboxIcon from "../../images/FruitAndVegetable/CheckboxIcon/UnCheckboxIcon";
import DownArrowBlack from "../../images/DownArrowBlack";
import CloseOpacity from "../../images/CloseOpacity";
import BodyFruit from "../../components/BodyFruit/BodyFruit";
import ThumbProduct from "../../images/BodyFruit/ThumbProduct";
import BlackStar from "../../images/BodyFruit/BlackStar";
import RingBlackStar from "../../images/BodyFruit/RingBlackStar";
import RightWhiteArrow from "../../images/BodyFruit/RightWhiteArrow";
import WishList from "../../images/BodyFruit/WishList";
import Footer from "../../components/Footer/Footer";
import { Link, useParams } from "react-router-dom";
import PageTitle from "../../components/PageTitle/PageTitle";
import { useSelector } from "react-redux";
import { IPayloadCatalog, IPayloadProduct } from "../../api/types";
import ContentPagination from "../../components/ContentPagination/ContentPagination";
import DrawerMenu from "../../components/DrawerMenu/DrawerMenu";
import GetAppIcon from "@material-ui/icons/GetApp";

const FruitAndVegetable = () => {
  const classes = useStyles();
  const [checkbox1, setCheckbox1] = useState<boolean>(false);
  const [checkbox2, setCheckbox2] = useState<boolean>(true);
  const [checkbox3, setCheckbox3] = useState<boolean>(true);
  const [gridView, setGridView] = useState<boolean>(false);
  const [pageNumber, setPageNumber] = useState<number>(1);
  const products = useSelector((state: any) => state.product);
  const catagories = useSelector((state: any) => state.catagory);
  const _id = useParams<{ _id: string }>()._id;
  let listProduct = products?.filter(
    (item: IPayloadProduct) => item.categoryID === _id
  );
  let catagory = catagories?.find((item: IPayloadCatalog) => {
    return item._id === _id;
  });
  const handleChangeGridView = () => {
    setGridView(false);
  };

  const handleChangeListView = () => {
    setGridView(true);
  };
  const renderPageTitle = () => {
    return (
      <>
        <PageTitle page={["HomePage", catagory?.categoryName]} />
        <Grid container xs={12} className={classes["page-title"]}>
          <h1>{catagory?.categoryName}</h1>
          <div className={classes["page-sort"]}>
            <div onClick={handleChangeGridView}>
              <GridView />
              Grid View
            </div>
            <div onClick={handleChangeListView}>
              <ListView />
              List View
            </div>
            <div>
              <p>{listProduct.length}</p>
              <p>Products</p>
            </div>
          </div>
        </Grid>
      </>
    );
  };
  const StyledRadio = (props: any) => {
    return (
      <Radio
        disableRipple
        color="default"
        checkedIcon={<CheckIcon />}
        icon={<UncheckIcon />}
        {...props}
      />
    );
  };

  const renderRadioOption = () => {
    return (
      <div className={classes["filtre-text"]}>
        <RadioGroup
          defaultValue="Filtre text 1"
          aria-label="gender"
          name="customized-radios"
        >
          <FormControlLabel
            value="Filtre text"
            control={<StyledRadio />}
            label="Filtre text"
          />
          <FormControlLabel
            value="Filtre text 1"
            control={<StyledRadio />}
            label="Filtre text"
          />
        </RadioGroup>
      </div>
    );
  };

  const handleChange = (state: boolean, func: Function) => {
    func(!state);
  };

  const formControlLable = (state: boolean, func: Function, text: string) => {
    return (
      <FormGroup row>
        <FormControlLabel
          control={
            <Checkbox
              icon={<UnCheckboxIcon />}
              checkedIcon={<CheckboxIcon />}
              checked={state}
              onChange={() => handleChange(state, func)}
            />
          }
          label={text}
        />
      </FormGroup>
    );
  };

  const renderCheckboxOption = (
    state: boolean,
    func: Function,
    text: string
  ) => {
    return (
      <div className={classes["nbm-div"]}>
        {formControlLable(state, func, text)}
        <span>Nbm</span>
      </div>
    );
  };

  const renderCheckboxOptionSelect = () => {
    return (
      <div className={classes["option-checkbox-select"]}>
        <div>
          {formControlLable(checkbox3, setCheckbox3, "Filtre")}
          <span>12</span>
        </div>
        <div className={classes["select-option"]}>
          Select <DownArrowBlack />
        </div>
      </div>
    );
  };

  const renderSelectOption = () => {
    return (
      <Grid container xs={12}>
        <Grid item xs={12} className={classes["wrapper-option-div"]}>
          {renderRadioOption()}
          {renderCheckboxOption(checkbox1, setCheckbox1, "Filtre")}
          {renderCheckboxOption(checkbox2, setCheckbox2, "Filtre")}
          {renderCheckboxOptionSelect()}
        </Grid>
        <Grid item xs={6} className={classes["selected-file"]}>
          <span>Applied filtres:</span>
          <p className={classes["selected-span"]}>
            Selected Filtre <CloseOpacity />
          </p>
          <p className={classes["selected-span"]}>
            Selected Filtre <CloseOpacity />
          </p>
        </Grid>
      </Grid>
    );
  };
  const layoutSelectOptions = () => {
    return (
      <>
        <Grid container xs={12} className={classes["wrapper-option"]}>
          <Grid item xs={6} className={classes["item1"]}>
            {renderCheckboxOption(checkbox1, setCheckbox1, "Filtre")}
          </Grid>

          <Grid item xs={6} className={classes["item2"]}>
            {renderRadioOption()}
          </Grid>
          <Grid item xs={6} className={classes["item3"]}>
            {renderCheckboxOption(checkbox2, setCheckbox2, "Filtre")}
          </Grid>
          <Grid item xs={6} className={classes["item4"]}>
            {renderCheckboxOptionSelect()}
          </Grid>
        </Grid>
        <Grid item xs={12} className={classes["selected-file-item"]}>
          <span>Applied filtres:</span>
          <p className={classes["selected-span"]}>
            Selected Filtre <CloseOpacity />
          </p>
          <p className={classes["selected-span"]}>
            Selected Filtre <CloseOpacity />
          </p>
        </Grid>
      </>
    );
  };
  const renderSelectOptionRespon = () => {
    return (
      <div className={classes["selected-file-respon"]}>
        <DrawerMenu
          drawerType="top"
          listIcon={<GetAppIcon />}
          content={layoutSelectOptions}
        />
      </div>
    );
  };

  const renderStars = (stars: number, ringStars: number) => {
    let arr = [];
    for (let i = 0; i < stars; i++) {
      if (i < ringStars) arr.push(<BlackStar />);
      else arr.push(<RingBlackStar />);
    }
    return arr;
  };

  const renderProduct = (item: IPayloadProduct) => {
    return (
      <div className={classes["product-item"]}>
        {/* <ThumbProduct /> */}
        <img
          src={item.image}
          alt=""
          style={{ width: "30%", borderRadius: "12px" }}
        />
        <div className={classes["contain-product-infor"]}>
          <div>
            <h2>{item.productTitle}</h2>
            <p>{item.spaceforDescription}</p>
            <div className={classes["contain-stars"]}>
              {renderStars(5, item.stars)}
            </div>
            <div className={classes["wrapper-product-infor"]}>
              <div className={classes["product-infor"]}>
                <div>
                  <p>Fresheness</p>
                  <p>Farm</p>
                  <p>Delivery</p>
                  <p>Stocks</p>
                </div>
                <div>
                  <p>
                    <span>New</span> (Extra fresh)
                  </p>
                  <p>Grocery Tarm Fields</p>
                  <p>Europe</p>
                  <p>320 pcs</p>
                </div>
              </div>
            </div>
          </div>
          <div className={classes["product-price"]}>
            <h2>{item.price} USD</h2>
            <h3>{item.price * (100 - item.discount)} USD</h3>
            <p>{item.shipping}</p>
            <p>Delivery in 1 day</p>
            <Link to="/detailsProduct">
              <button>
                Product Detail <RightWhiteArrow />
              </button>
            </Link>
            <button>
              <WishList /> Add to wish list
            </button>
          </div>
        </div>
      </div>
    );
  };

  const renderListProduct = () => {
    return (
      <Grid item xs={12} md={12} lg={9} className={classes["wrapper-body-div"]}>
        {listProduct.map((item: IPayloadProduct) => {
          return renderProduct(item as IPayloadProduct);
        })}
      </Grid>
    );
  };

  const bodyFruit = () => {
    return (
      <Grid container xs={12} className={classes["wrapper-body"]}>
        <BodyFruit />
        <Grid
          item
          sm={12}
          md={12}
          lg={9}
          className={classes["wrapper-body-div"]}
        >
          {!gridView ? (
            <ContentPagination
              products={listProduct}
              setPageNumber={setPageNumber}
              renderStars={renderStars}
            />
          ) : (
            renderListProduct()
          )}
        </Grid>
      </Grid>
    );
  };

  return (
    <div className={classes.root}>
      {renderPageTitle()}
      {renderSelectOption()}
      {bodyFruit()}
      {renderSelectOptionRespon()}
      <Footer />
    </div>
  );
};

export default FruitAndVegetable;
