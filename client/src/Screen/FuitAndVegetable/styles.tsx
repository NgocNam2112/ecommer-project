import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "0 45px",
  },
  "page-route": {
    height: 48,
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "12px",
    lineHeight: "16px",
    display: "flex",
    alignItems: "center",
    "& p": {
      "&:first-child": {
        color: "#A9A9A9",
      },
    },
    "& span": {
      margin: "0 8px",
    },
  },
  "page-title": {
    height: 64,
    display: "flex",
    justifyContent: "space-between",

    "& > h1": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "32px",
      lineHeight: "48px",
      color: "#151515",
    },
  },
  "page-sort": {
    display: "flex",
    justifyContent: "space-between",
    width: 275,
    height: 18,
    margin: "auto 0",
    "& div": {
      display: "flex",
      justifyContent: "space-between",
      fontFamily: "Open Sans",
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "12px",
      lineHeight: "16px",
      color: "#151515",
      cursor: "pointer",
      "& p": {
        marginLeft: 12,
        "&:first-child": {
          color: "#6A983C",
        },
      },
      "& svg": {
        marginRight: 5.33,
      },
    },
  },
  "wrapper-option-div": {
    display: "flex",
    marginTop: 24,
    "& > div": {
      marginRight: 16,
    },
  },
  "wrapper-option": {
    padding: "30px 30px 0 30px",
    "& > div": {
      marginBottom: 20,
    },
  },
  "selected-file-respon": {
    display: "none",
    position: "absolute",
    top: 40,
    left: "50%",
    transform: "translate(-50%, 0)",
  },
  "filtre-text": {
    width: 236,
    height: 42,
    display: "flex",
    alignItems: "center",
    background: "#F9F9F9",
    border: "1px solid #D1D1D1",
    boxSizing: "border-box",
    borderRadius: "12px",
    "& div": {
      flexDirection: "row",
      flexWrap: "nowrap",
      margin: "0 auto",
      "& label": {
        marginLeft: 0,
        marginRight: 0,
        "& > span": {
          padding: 0,
          marginRight: 10.19,
          fontFamily: "Open Sans",
          fontStyle: "normal",
          fontWeight: "normal",
          fontSize: "14px",
          lineHeight: "19px",
          color: "#151515",
        },
      },
    },
  },
  "nbm-div": {
    width: 151,
    height: 42,
    background: "#F9F9F9",
    border: "1px solid #D1D1D1",
    boxSizing: "border-box",
    borderRadius: "12px",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
    "& div": {
      "& span": {
        fontFamily: "Open Sans",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "14px",
        lineHeight: "19px",
        color: "#151515",
      },
      "& label": {
        marginLeft: 0,
        marginRight: 0,
        "& span": {
          "&:first-child": {
            padding: 0,
            marginRight: 5,
          },
        },
      },
    },
    "& span": {
      fontFamily: "Open Sans",
      fontStyle: "normal",
      fontSize: "14px",
      lineHeight: "19px",
      color: "#151515",
      fontWeight: 600,
    },
  },
  "option-checkbox-select": {
    width: 235,
    height: 42,
    background: "#F9F9F9",
    border: "1px solid #D1D1D1",
    boxSizing: "border-box",
    borderRadius: "12px",
    display: "flex",
    alignItems: "center",
    "& div": {
      "&:first-child": {
        display: "flex",
        alignItems: "center",
        "& label": {
          marginLeft: 18,
          marginRight: 0,
          "& span": {
            "&:first-child": {
              marginRight: 10,
            },
            padding: 0,
            fontFamily: "Open Sans",
            fontStyle: "normal",
            fontWeight: "normal",
            fontSize: "14px",
            lineHeight: "19px",
            color: "#151515",
          },
        },
      },
      "& > span": {
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "600",
        fontSize: "12px",
        lineHeight: "18px",
        color: "#151515",
        padding: "3px 18px 0 16px",
        borderRight: "1px solid #D1D1D1",
      },
    },
  },
  "select-option": {
    display: "flex",
    alignItems: "center",
    marginLeft: 24,
    fontFamily: "Poppins",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "12px",
    lineHeight: "18px",
    color: "#151515",
    "& svg": {
      marginLeft: 7.61,
    },
  },
  "selected-file": {
    display: "flex",
    marginTop: 16,
    "& span": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "12px",
      lineHeight: "18px",
      color: "#A9A9A9",
    },
  },
  "selected-span": {
    fontFamily: "Poppins",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "12px",
    lineHeight: "18px",
    color: "#6A983C !important",
    padding: "0 8px",
    background: "#F4F8EC",
    borderRadius: "12px",
    alignItems: "center",
    display: "flex",
    width: 108,
    height: 18,
    marginLeft: 12,
    "& svg": {
      marginLeft: 6.8,
    },
  },
  "wrapper-body": {
    marginTop: 64,
  },
  "wrapper-body-div": {
    margin: "0 auto",
  },
  "product-item": {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    border: "1px solid #D1D1D1",
    boxSizing: "border-box",
    borderRadius: "12px",
    marginBottom: 32,
    "& > div": {
      "& h2": {
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "600",
        fontSize: "18px",
        lineHeight: "27px",
        color: "#151515",
        marginTop: 32,
      },
      "& > p": {
        fontFamily: "Open Sans",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "12px",
        lineHeight: "16px",
        color: "#575757",
        margin: "4px 0 10.3px 0",
      },
    },
  },
  "contain-product-infor": {
    width: "65%",
    display: "flex",
    justifyContent: "space-between",
    "& > div": {
      "& h2": {
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "600",
        fontSize: "18px",
        lineHeight: "27px",
        color: "#151515",
        marginTop: 32,
      },
      "& > p": {
        fontFamily: "Open Sans",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "12px",
        lineHeight: "16px",
        color: "#575757",
        margin: "4px 0 10.3px 0",
      },
    },
  },
  "wrapper-product-infor": {
    marginTop: 27,
  },
  "product-infor": {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 12,
    "& div": {
      "& p": {
        fontFamily: "Open Sans",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "14px",
        lineHeight: "19px",
        color: "#A9A9A9",
        marginBottom: 12,
        "& span": {
          color: "#6A983C",
        },
      },
      "&:last-child": {
        marginLeft: 32,
      },
    },
  },
  "product-price": {
    display: "flex",
    flexDirection: "column",
    marginRight: 32,
    "& h2": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "18px",
      lineHeight: "27px",
      color: "#151515",
    },
    "& h3": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "12px",
      lineHeight: "18px",
      textDecorationLine: "line-through",
      color: "#A9A9A9",
      marginBottom: 16,
    },
    "& p": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: 600,
      fontSize: "12px",
      lineHeight: "18px",
      margin: "0 !important",
      color: "#A9A9A9",
      "&:last-child": {
        fontWeight: "normal",
      },
    },
    "& a": {
      textDecoration: "none",
      "& button": {
        width: "164px",
        height: "47px",
        background: "#6A983C",
        border: "2px solid #46760A",
        boxSizing: "border-box",
        borderRadius: "12px",
        marginTop: 26,
        outline: "none",
        cursor: "pointer",
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "bold",
        fontSize: "15px",
        lineHeight: "22px",
        color: "#FFFFFF",
      },
    },
    "& > button": {
      width: "164px",
      height: "47px",
      marginTop: 12,
      background: "#F5F5F5",
      borderRadius: "12px",
      fontSize: "15px",
      border: "none",
      color: "#151515",
      outline: "none",
      cursor: "pointer",
    },
  },
  [theme.breakpoints.down(900)]: {
    "selected-file-item": {
      marginBottom: 20,
      padding: "0 0 0 30px",
      display: "flex",
      marginTop: 16,
      "& span": {
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "600",
        fontSize: "12px",
        lineHeight: "18px",
        color: "#A9A9A9",
      },
    },

    "selected-file-respon": {
      display: "block",
    },
    "wrapper-option-div": {
      display: "none",
    },
    "selected-file": {
      display: "none",
    },
  },
  [theme.breakpoints.down(780)]: {
    "wrapper-product-infor": {
      display: "none",
    },
    "product-item": {
      "& > svg": {
        marginLeft: 25,
      },
    },
    "product-price": {
      "& p": {
        display: "none",
      },
      "& button": {
        "&:first-child": {
          marginTop: 0,
        },
      },
    },
    "contain-product-infor": {
      display: "flex",
      flexDirection: "column",
      justifyContent: "space-around",
      marginLeft: 50,
      "& div": {
        "& h2": {
          marginTop: 0,
        },
      },
    },
    "contain-stars": {
      display: "none",
    },
  },
  [theme.breakpoints.down(560)]: {
    "selected-file-respon": {
      right: "50%",
      top: "55px",
    },
  },
  [theme.breakpoints.down(492)]: {
    "selected-file-respon": {
      top: 10,
    },
    "wrapper-option": {
      display: "grid",
      gridTemplateAreas: "'a' 'c' 'b' 'd'",
      gap: "10px",
    },
    item1: {
      gridArea: "a",
    },
    item2: {
      gridArea: "b",
    },
    item3: {
      gridArea: "c",
    },
    item4: {
      gridArea: "d",
    },
  },
}));

export default useStyles;
