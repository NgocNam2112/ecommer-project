import React, { useState } from "react";
import RightArrowBlack from "../../images/HomPage/RightArrowBlack";
import useStyles from "./styles";
import Grid from "@material-ui/core/Grid";
import Avatar from "../../images/HomPage/Avatar/Ellipse_1.png";
import Slider from "react-slick";
import RightArrowSlide from "../../images/HomPage/RightArrowSlide";
import LeftArrowSlide from "../../images/HomPage/LeftArrowSlide";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import LeftArrowGreen from "../../images/HomPage/LeftArrowGreen";
import MainBlog from "../../images/HomPage/Blog/MainBlog.png";
import AvatarBlog from "../../images/HomPage/Avatar/Ellipse_3.png";
import Vegetable from "../../images/HomPage/Blog/Vegetable.png";
import Salat from "../../images/HomPage/Blog/Salat.png";
import Footer from "../../components/Footer/Footer";
import ProductItem from "../../components/ProductItem/ProductItem";
import SpaceForBlog from "../../components/SpaceForBlog/SpaceForBlog";
import BlogPostItem from "../../components/SpaceForBlog/BlogPostItem/BlogPostItem";
import { useSelector } from "react-redux";
import ShowMoreProduct from "../../components/ShowMoreProduct/ShowMoreProduct";
import ContentPaginate from "../../components/ContentPagination/ContentPagination";

interface Data {
  h1: string;
  li: Record<string, any>;
}

function SampleNextArrow(props: any) {
  const { onClick, className } = props;
  return (
    <div className={className} onClick={onClick} style={{ right: 0 }}>
      <RightArrowSlide />
    </div>
  );
}

function SamplePrevArrow(props: any) {
  const { onClick, className } = props;
  return (
    <div
      className={className}
      onClick={onClick}
      style={{ left: 0, zIndex: 10 }}
    >
      <LeftArrowSlide />
    </div>
  );
}

const HomePage = () => {
  const classes = useStyles();
  const products = useSelector((state: any) => state.product);
  const [isLoad, setIsLoad] = useState<boolean>(false);
  const [productPerPage] = useState(8);
  const [pageNumber, setPageNumber] = useState<number>(1);
  const indexOfLastPage = pageNumber * productPerPage;
  const indexOfFirstPage = (pageNumber - 1) * productPerPage;
  const currentProduct = products.slice(indexOfFirstPage, indexOfLastPage);
  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 1530,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 816,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  const categories: Data = {
    h1: "Category menu",
    li: [
      "Bakery",
      "Fruit and vegetables",
      "Meat and fish",
      "Drinks",
      "Kitchen",
    ],
  };
  const bestSelling: Data = {
    h1: "Best selling products",
    li: ["Kitchen", "Meat and fish", "Special nutrition", "Pharmacy", "Baby"],
  };
  const bestFromFarmer: Data = {
    h1: "Best from Farmers",
    li: ["Carrots", "Tomatoes", "Potatoes", "Chicken", "Pork"],
  };
  const catalogGridView = (value: Data) => {
    return (
      <div>
        <h1>{value.h1}</h1>
        <ul>
          {value.li.map((item: string) => {
            return <li>{item}</li>;
          })}
        </ul>
        <button>
          More categories
          <RightArrowBlack />
        </button>
      </div>
    );
  };
  const spaceForHeading = (style: string) => {
    return (
      <div className={`${classes["space-item"]} ${classes[style]}`}>
        <p>Banner subfocus</p>
        <h1>Space for heading</h1>
        <button>Read recepies</button>
      </div>
    );
  };
  const feedBack = () => {
    return (
      <div className={classes["customer-feedback"]}>
        <p>
          “ This is an super space for your customers qoute. Don’t worry it
          works smooth as pie. You will get all what you need by writiing a text
          here “
        </p>
        <p>Name and Surname</p>
        <img src={Avatar} alt="" />
      </div>
    );
  };

  const renderTitle = (h2: string, button: string) => {
    return (
      <div className={classes["title-slide"]}>
        <h2>{h2}</h2>
        <button>
          {button}
          <LeftArrowGreen />
        </button>
      </div>
    );
  };

  const renderSlide = () => {
    return (
      <div className={classes["wrapper-feedback"]}>
        {renderTitle("Our customers says", "Button")}
        <Slider {...settings} className={classes["custom-slider"]}>
          {feedBack()}
          {feedBack()}
          {feedBack()}
          {feedBack()}
          {feedBack()}
        </Slider>
      </div>
    );
  };
  const headLine = () => {
    return (
      <div>
        {renderTitle("Section Headline", "Button")}
        <div className={classes["list-item"]}>
          <ProductItem
            titleProduct="Product Title"
            description="Space for a small product description "
            price={148}
            discount={36}
            key={1}
          />
          <ProductItem
            titleProduct="Product Title"
            description="Space for a small product description "
            price={148}
            discount={36}
            key={2}
          />
          <ProductItem
            titleProduct="Product Title"
            description="Space for a small product description "
            price={148}
            discount={36}
            key={3}
          />
          <ProductItem
            titleProduct="Product Title"
            description="Space for a small product description "
            price={148}
            discount={36}
            key={4}
          />
          <ProductItem
            titleProduct="Product Title"
            description="Space for a small product description "
            price={148}
            discount={36}
            key={5}
            image={
              <img
                src="https://media.istockphoto.com/photos/food-backgrounds-table-filled-with-large-variety-of-food-picture-id1155240408?k=6&m=1155240408&s=612x612&w=0&h=SEhOUzsexrBBtRrdaLWNB6Zub65Dnyjk7vVrTk1KQSU="
                alt=""
                onLoad={LoadingImage}
                onError={ErrorLoading}
                style={{ width: 236, height: 180, borderRadius: 12 }}
              />
            }
            isLoad={isLoad}
          />
        </div>
      </div>
    );
  };

  const listBlog = () => {
    return (
      <div className={classes["list-blog"]}>
        <div>
          <h2>Salat is kinda good start to your morning routines</h2>
          <div>
            <span>Author</span>
            <span>15. 6. 2020</span>
          </div>
        </div>
        <img src={Salat} alt="" />
      </div>
    );
  };

  const readOutBlog = () => {
    return (
      <div className={classes["read-our-blog"]}>
        {renderTitle("Read our Blog posts", "Go to Blog")}
        <div className={classes["wrapper-blog"]}>
          <SpaceForBlog
            mainblog={MainBlog}
            tags="Dinner tips"
            blogheadline="Our chef tips for a great and tasty dinner ready in 20 minutes"
            avatarauthor={AvatarBlog}
            date="17. 6. 2020"
          />
          <BlogPostItem
            blogitemimage={Vegetable}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
          <div className={classes["wrapper-blog-list"]}>
            {listBlog()}
            {listBlog()}
            {listBlog()}
          </div>
        </div>
      </div>
    );
  };

  /**
   * Check image loading
   */

  const LoadingImage = () => {
    setIsLoad(true);
  };
  const ErrorLoading = () => {
    setIsLoad(false);
  };
  /**
   * paginate page
   */
  return (
    <div className={classes.root}>
      <Grid container spacing={2} className={classes["wrapper-menu"]}>
        <Grid item xs={3} className={classes.menu}>
          {catalogGridView(bestSelling)}
          {catalogGridView(categories)}
          {catalogGridView(bestFromFarmer)}
        </Grid>
        <Grid item xs={12} md={12} lg={9} sm={12}>
          <Grid item xs={12} className={classes["wrapper-block"]}>
            {spaceForHeading("first-heading")}
            {spaceForHeading("second-heading")}
          </Grid>

          <ContentPaginate
            products={currentProduct}
            setPageNumber={setPageNumber}
          />
        </Grid>
      </Grid>
      {currentProduct.length > 0 ? (
        <ShowMoreProduct
          productLength={products.length}
          productPerPage={productPerPage}
          currentNumber={pageNumber}
          pageNumber={pageNumber}
        />
      ) : (
        <ShowMoreProduct
          productLength={currentProduct.length}
          productPerPage={productPerPage}
          currentNumber={pageNumber}
          pageNumber={pageNumber}
        />
      )}

      {renderSlide()}
      {readOutBlog()}
      <Footer />
    </div>
  );
};

export default HomePage;
