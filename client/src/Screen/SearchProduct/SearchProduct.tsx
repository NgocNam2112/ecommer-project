import { Grid } from "@material-ui/core";
import { Switch, Route } from "react-router-dom";
import RightArrowBlack from "../../images/HomPage/RightArrowBlack";
import ContentPaginate from "../../components/ContentPagination/ContentPagination";
import useStyles from "./styles";
import ShowMoreProduct from "../../components/ShowMoreProduct/ShowMoreProduct";
import { useEffect, useState } from "react";
import Footer from "../../components/Footer/Footer";
import { IPayloadProduct } from "../../api/types";
import NotFound from "../NotFound/NotFound";
interface Data {
  h1: string;
  li: Record<string, any>;
}
interface IProps {
  searchProduct: Array<IPayloadProduct>;
}

const SearchProduct: React.FC<IProps> = ({ searchProduct }) => {
  const classes = useStyles();
  const [productPerPage] = useState(8);
  const [pageNumber, setPageNumber] = useState<number>(1);
  const indexOfLastPage = pageNumber * productPerPage;
  const indexOfFirstPage = (pageNumber - 1) * productPerPage;
  const currentProduct = searchProduct.slice(indexOfFirstPage, indexOfLastPage);
  const categories: Data = {
    h1: "Category menu",
    li: [
      "Bakery",
      "Fruit and vegetables",
      "Meat and fish",
      "Drinks",
      "Kitchen",
    ],
  };
  const bestSelling: Data = {
    h1: "Best selling products",
    li: ["Kitchen", "Meat and fish", "Special nutrition", "Pharmacy", "Baby"],
  };
  const bestFromFarmer: Data = {
    h1: "Best from Farmers",
    li: ["Carrots", "Tomatoes", "Potatoes", "Chicken", "Pork"],
  };
  const catalogGridView = (value: Data) => {
    return (
      <div>
        <h1>{value.h1}</h1>
        <ul>
          {value.li.map((item: string) => {
            return <li>{item}</li>;
          })}
        </ul>
        <button>
          More categories
          <RightArrowBlack />
        </button>
      </div>
    );
  };
  useEffect(() => {
    console.log("search products", searchProduct);
  }, [searchProduct]);
  return (
    <div className={classes.root}>
      {searchProduct.length !== 0 ? (
        <>
          <Grid container spacing={2} className={classes["wrapper-menu"]}>
            <Grid item xs={3} className={classes.menu}>
              {catalogGridView(bestSelling)}
              {catalogGridView(categories)}
              {catalogGridView(bestFromFarmer)}
            </Grid>
            <Grid item xs={12} md={12} lg={9} sm={12}>
              <ContentPaginate
                products={currentProduct}
                setPageNumber={setPageNumber}
              />
            </Grid>
          </Grid>
          <ShowMoreProduct
            productLength={searchProduct.length}
            productPerPage={productPerPage}
            currentNumber={pageNumber}
            pageNumber={pageNumber}
          />
        </>
      ) : (
        <NotFound />
      )}
      <Footer />
    </div>
  );
};

export default SearchProduct;
