import React from "react";
import useStyles from "./styles";

const SignUp = () => {
  const classes = useStyles();
  return (
    <>
      <div className={classes.login}>
        <h2 className={classes.active}> sign in </h2>

        <h2 className={classes.nonactive}> sign up </h2>
        <form>
          <input type="text" className={classes.text} name="username" />
          <span>username</span>

          <br />

          <br />

          <input type="password" className={classes.text} name="password" />
          <span>password</span>
          <br />

          <input
            type="checkbox"
            id="checkbox-1-1"
            className={classes["custom-checkbox"]}
          />
          <label htmlFor="checkbox-1-1">Keep me Signed in</label>

          <button className={classes.signin}>Sign In</button>

          <hr />

          <p>Forgot Password?</p>
        </form>
      </div>
    </>
  );
};

export default SignUp;
