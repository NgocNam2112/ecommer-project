import React from "react";
export default function Test() {
  const renderStar = (i: number) => {
    return (
      <img
        src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Star_full.svg/1200px-Star_full.svg.png"
        alt=""
        style={{ width: "100px" }}
        key={i}
      />
    );
  };

  const deepStar = (i: number) => {
    return (
      <img
        src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Empty_Star.svg/1200px-Empty_Star.svg.png"
        alt=""
        key={i}
        style={{ width: 100 }}
      />
    );
  };

  const renderRating = (amount: number, stars: number) => {
    let arr = [];
    for (let i = 0; i < amount; i++) {
      if (i < stars) {
        console.log(i);
        arr.push(renderStar(i));
      } else {
        arr.push(deepStar(i));
      }
    }
    return arr;
  };
  return <div className="App">{renderRating(5, 3)}</div>;
}
