import * as api from "../api/index";
import { GET_LIST_CATALOG } from "../constants/actionTypes";

//catagory router

export const getListCatalog = () => async (dispatch: any) => {
  try {
    const { data } = await api.fetCatagory();
    dispatch({ type: GET_LIST_CATALOG, payload: data });
  } catch (error) {
    console.log("error here", error);
  }
};
