import { CREATE, DELETE, UPDATE, FETCH_ALL } from "../constants/actionTypes";

import * as api from "../api/index";

export const getProducts = () => async (dispatch: any) => {
  try {
    const { data } = await api.fetchProducts();
    dispatch({ type: FETCH_ALL, payload: data });
  } catch (err) {
    console.log("error here", err.message);
  }
};
