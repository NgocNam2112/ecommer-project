import * as api from "../api/index";
import { IInfor } from "../api/types";

export const registerUser = async (info: IInfor) => {
  try {
    const data = await api.registerAccount(info);
    console.log("data", data);
  } catch (error) {}
};
