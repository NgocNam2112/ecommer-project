import axios from "axios";
import { IInfor } from "./types";

const BASE_URL = "http://localhost:5000";

//catagory router
export const fetCatagory = () => axios.get(`${BASE_URL}/category`);

// product route
export const fetchProducts = () => axios.get(`${BASE_URL}/productions`);
// user route
export const registerAccount = (info: IInfor) =>
  axios.post(`${BASE_URL}/auth/register`, info);
