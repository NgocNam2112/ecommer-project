import { Grid } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import ProductItem from "../ProductItem/ProductItem";
import useStyles from "./styles";

interface IProps {
  products: Array<any>;
  setPageNumber: Function;
  renderStars?: (stars: number, ringStars: number) => JSX.Element[];
}

const ContentPagination: React.FC<IProps> = ({
  products,
  setPageNumber,
  renderStars,
}) => {
  const classes = useStyles();
  const [isLoad, setIsLoad] = useState<boolean>(false);
  const query = new URLSearchParams(useLocation().search);
  const LoadingImage = () => {
    setIsLoad(true);
  };
  const ErrorLoading = () => {
    setIsLoad(false);
  };

  useEffect(() => {
    LoadingImage();
  });
  useEffect(() => {
    if (query.get("page") === null) {
      setPageNumber(1);
    } else {
      setPageNumber(parseInt(query.get("page") as string));
    }
  });
  return (
    <Grid item xs={12} sm={12} md={12} className={classes["wrapper-product"]}>
      {products.map((item: any) => {
        return (
          <ProductItem
            titleProduct={item.productTitle}
            description={item.spaceforDescription}
            price={item.price}
            image={
              <img
                src={item.image}
                alt=""
                onLoad={LoadingImage}
                onError={ErrorLoading}
                style={{ width: 237, height: 180, borderRadius: 12 }}
              />
            }
            id={item._id}
            isLoad={isLoad}
            key={item._id}
            discount={item.discount}
            stars={
              renderStars ? renderStars(5, item.stars as number) : undefined
            }
          />
        );
      })}
    </Grid>
  );
};

export default ContentPagination;
