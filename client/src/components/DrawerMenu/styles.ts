import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  list: {
    width: 250,
  },
  fullList: {
    width: "auto",
  },
  "catalog-menu": {
    listStyle: "none",
    "& li": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: 700,
      fontSize: "15px",
      lineHeight: "22px",
      color: "#151515",
      margin: "auto 0",
      marginRight: 43.63,
      cursor: "pointer",
      marginTop: 20,
      marginLeft: 20,
      "& a": {
        textDecoration: "none",
        color: "#151515",
      },
    },
    "& svg": {
      marginBottom: 2,
    },
  },
}));

export default useStyles;
