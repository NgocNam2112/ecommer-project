import React, { useEffect, useState } from "react";
import TradeName from "../../images/Header/TradeName";
import useStyle from "./styles";
import Paper from "@material-ui/core/Paper";
import InputBase from "@material-ui/core/InputBase";
import AllCategories from "../../images/Header/AllCategories";
import DownArrowGreen from "../../images/Header/DownArrowGreen";
import Search from "../../images/Header/Search";
import User from "../../images/Header/User";
import Basket from "../../images/Header/Basket";
import DownArrowThinner from "../../images/Header/DownArrowThinner";
import { Link } from "react-router-dom";
import { Dialog } from "@material-ui/core";
import Login from "../Login/Login";
import { getListCatalog } from "../../actions/catagoryAction";
import { useDispatch, useSelector } from "react-redux";
import { IPayloadCatalog, IPayloadProduct } from "../../api/types";
import ListIcon from "@material-ui/icons/List";
import DrawerMenu from "../DrawerMenu/DrawerMenu";

interface IProps {
  getSearchProduct: (item: Array<IPayloadProduct>) => void;
}

const Header: React.FC<IProps> = ({ getSearchProduct }) => {
  const classes = useStyle();

  const [open, setOpen] = useState(false);
  const [inputValue, setInputValue] = useState<string>("");
  const [isShowPopUp, setIsShowPopUp] = useState<boolean>(false);
  const dispatch = useDispatch();
  const catalog: Array<IPayloadCatalog> = useSelector(
    (state: any) => state.catagory
  );
  const products = useSelector((state: any) => state.product);
  let filterProduct = products.filter((item: IPayloadProduct) => {
    if (inputValue === "") {
      return item;
    } else if (
      item.productTitle
        .toLocaleLowerCase()
        .includes(inputValue.toLocaleLowerCase())
    ) {
      return item;
    }
  });
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  useEffect(() => {
    dispatch(getListCatalog());
  }, [dispatch]);

  const shopInfor = () => {
    return (
      <div className={classes["shop-infor"]}>
        <div>
          <p>Chat with us</p>
          <p>+420 336 775 664</p>
          <p>info@freshnesecom.com</p>
        </div>
        <div>
          <p>Blog</p>
          <p>About Us</p>
          <p>Careers</p>
        </div>
      </div>
    );
  };
  const userPlace = () => {
    return (
      <div className={classes["trade-name-icon"]}>
        <button
          onClick={() => {
            handleClickOpen();
            setIsShowPopUp(false);
          }}
          className={classes["show-dialog-login"]}
        >
          <User />
        </button>
        <Link to="/checkout">
          <Basket />
        </Link>
      </div>
    );
  };
  const handleChangeInput = (e: any) => {
    setInputValue(e.target.value);
    setIsShowPopUp(true);
  };

  const listCatalog = () => {
    return (
      <div className={classes["catalog"]}>
        <ul className={classes["catalog-menu"]}>
          {catalog.map((item: IPayloadCatalog) => {
            return (
              <li>
                <Link to={`/list-product/${item.categoryName}/${item._id}`}>
                  {item.categoryName} <DownArrowThinner />
                </Link>
              </li>
            );
          })}
        </ul>
      </div>
    );
  };
  const onHandleClickItem = (val: IPayloadProduct) => {
    setInputValue(val.productTitle);
    setIsShowPopUp(false);
  };
  const showPopUp = (value: Array<IPayloadProduct>) => {
    if (value.length !== 0) {
      return (
        <div className={classes["popup-products"]}>
          {value.map((item: IPayloadProduct) => {
            return (
              <div
                className={classes["item-popup"]}
                onClick={() =>
                  onHandleClickItem(item as unknown as IPayloadProduct)
                }
              >
                {item.productTitle}
              </div>
            );
          })}
        </div>
      );
    } else {
      return null;
    }
  };

  const searchEvent = () => {
    getSearchProduct(filterProduct);
    setIsShowPopUp(false);
  };

  const tradeName = () => {
    return (
      <div className={classes["shop-trade-name"]}>
        <Link to="/">
          <TradeName />
        </Link>
        <div className={classes["contain-autosearch"]}>
          <Paper component="form" className={classes.input}>
            <div className={classes["custom-categories-input"]}>
              <AllCategories />
              <DownArrowGreen />
            </div>
            <InputBase
              placeholder="Search Products, categories ..."
              inputProps={{ "aria-label": "search products" }}
              className={classes["input-base"]}
              onChange={(e) => handleChangeInput(e)}
              value={inputValue}
            />
            <div
              className={classes["custom-search-icon"]}
              onClick={() => {
                searchEvent();
                setInputValue("");
              }}
            >
              {inputValue !== "" ? (
                <Link to="/search-product">
                  <Search />
                </Link>
              ) : (
                <Search />
              )}
            </div>
          </Paper>
          {isShowPopUp ? showPopUp(filterProduct) : null}
        </div>
        <div className={classes["custom-use-place"]}>{userPlace()}</div>
      </div>
    );
  };
  // layout list catagory when reponsiving.
  const contentDrawer = () => {
    return (
      <ul className={classes["catalog-menu-responsive"]}>
        {catalog.map((item: IPayloadCatalog) => {
          return (
            <li>
              <Link to={`/list-product/${item.categoryName}/${item._id}`}>
                {item.categoryName} <DownArrowThinner />
              </Link>
            </li>
          );
        })}
      </ul>
    );
  };
  const listCatalogRepon = () => {
    return (
      <div className={classes["list-catalog-icon"]}>
        <div className={classes["list-catalog-container"]}>
          <DrawerMenu
            listIcon={<ListIcon />}
            drawerType="left"
            content={contentDrawer}
          />
          {userPlace()}
        </div>
      </div>
    );
  };
  useEffect(() => {
    if (inputValue === "") {
      setIsShowPopUp(false);
    }
  }, [inputValue]);
  return (
    <>
      {shopInfor()}
      {tradeName()}
      {listCatalog()}
      {listCatalogRepon()}
      <SimpleDialog open={open} onClose={handleClose} />
    </>
  );
};

function SimpleDialog(props: any) {
  const { onClose, selectedValue, open } = props;

  const handleClose = () => {
    onClose(selectedValue);
  };

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={open}
    >
      <Login />
    </Dialog>
  );
}

export default Header;
