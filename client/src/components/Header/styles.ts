import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  "shop-infor": {
    padding: "0 45px",
    display: "flex",
    justifyContent: "space-between",
    borderBottom: "1px solid #F9F9F9",
    "& div": {
      display: "flex",
      padding: "16px 0",
      "&:first-child": {
        "& p": {
          fontFamily: "Open Sans",
          fontStyle: "normal",
          fontWeight: "normal",
          fontSize: "12px",
          lineHeight: "16px",
          color: "#151515",
          marginRight: 33,
          "&:first-child": {
            color: "#6A983C",
          },
        },
      },
      "&:last-child": {
        fontFamily: "Open Sans",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "12px",
        lineHeight: "16px",
        color: "#6A983C",
        "& p": {
          marginLeft: 38,
        },
      },
    },
  },
  "shop-trade-name": {
    padding: "0 45px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 72,
  },
  "contain-autosearch": {
    width: "50%",
    display: "flex",
    flexDirection: "column",
    position: "relative",
  },
  input: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    height: 42,
    background: "#F9F9F9",
    border: "1px solid #D1D1D1",
    boxSizing: "border-box",
    borderRadius: "12px !important",
    boxShadow: "none !important",
    "& input": {
      marginLeft: "24px",
      fontSize: "14px",
      color: "#A9A9A9",
    },
  },
  iconButton: {
    padding: 10,
  },
  "custom-categories-input": {
    height: 23,
    width: "15%",
    padding: "0 24px 0 16px",
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
    borderRight: "1px solid #D1D1D1",
    "& svg": {
      "&:last-child": {
        marginLeft: 12.81,
      },
    },
  },
  "input-base": {
    width: "77%",
  },
  "popup-products": {
    width: "100%",
    maxHeight: 500,
    overflow: "scroll",
    padding: "10px 0 10px 20px",
    boxSizing: "border-box",
    border: "1px solid #D1D1D1",
    position: "absolute",
    backgroundColor: "#fff",
    zIndex: 10,
    top: 42,
    borderRadius: "12px",
  },
  "item-popup": {
    fontSize: "15px",
    fontStyle: "normal",
    fontFamily: "Poppins",
  },
  "custom-search-icon": {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    margin: "0 auto",
    cursor: "pointer",
    marginRight: 10,
    marginLeft: 10,
  },
  "custom-use-place": {
    display: "block",
  },
  "trade-name-icon": {
    "& svg": {
      cursor: "pointer",
      "&:last-child": {
        marginLeft: 47,
      },
    },
  },
  catalog: {
    padding: "0 45px",
    background: "#F9F9F9",
    marginTop: 48,
  },
  "catalog-menu": {
    display: "flex",
    listStyle: "none",
    padding: "16px 0",
    "& li": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: 700,
      fontSize: "15px",
      lineHeight: "22px",
      color: "#151515",
      margin: "auto 0",
      marginRight: 43.63,
      cursor: "pointer",
      "& a": {
        textDecoration: "none",
        color: "#151515",
      },
    },
  },
  "show-dialog-login": {
    background: "none",
    outline: "none",
    border: "none",
  },
  "list-catalog-icon": {
    display: "none",
    padding: "0 45px",
    background: "#F9F9F9",
    marginTop: 48,
  },
  "list-catalog-container": {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  "catalog-menu-responsive": {
    listStyle: "none",
    "& li": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: 700,
      fontSize: "15px",
      lineHeight: "22px",
      color: "#151515",
      margin: "auto 0",
      marginRight: 43.63,
      cursor: "pointer",
      marginTop: 20,
      marginLeft: 20,
      "& a": {
        textDecoration: "none",
        color: "#151515",
      },
    },
    "& svg": {
      marginBottom: 2,
    },
  },
  [theme.breakpoints.down(1280)]: {
    "custom-categories-input": {
      width: "25%",
    },
    "input-base": {
      width: "70%",
    },
  },
  [theme.breakpoints.down(1200)]: {
    catalog: {
      display: "none",
    },
    "list-catalog-icon": {
      display: "block",
    },
    "custom-use-place": {
      display: "none",
    },
    "shop-trade-name": {
      flexDirection: "column",
      alignItems: "stretch",
      marginTop: 20,
    },
    "contain-autosearch": {
      margin: "30px auto 0 auto",
    },
  },
  [theme.breakpoints.down(1000)]: {
    "contain-autosearch": {
      width: "70%",
    },
  },

  [theme.breakpoints.down(730)]: {
    "shop-infor": {
      "& div": {
        "&:last-child": {
          display: "none",
        },
      },
    },
    "contain-autosearch": {
      width: "80%",
    },
  },
  [theme.breakpoints.down(540)]: {
    "contain-autosearch": {
      width: "90%",
    },
    "custom-categories-input": {
      width: "30%",
    },
  },
  [theme.breakpoints.down(490)]: {
    "shop-infor": {
      "& div": {
        display: "none",
      },
    },
    "contain-autosearch": {
      width: "90%",
    },
  },
}));

export default useStyles;
