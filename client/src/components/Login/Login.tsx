import React, { useState } from "react";
import useStyles from "./styles";

const Login = () => {
  const [signIn, setSignIn] = useState<boolean>(true);
  const styles = useStyles();
  return (
    <div className={styles.signin}>
      <div className={styles["back-img"]}>
        <div className={styles["sign-in-text"]}>
          <h2
            className={signIn ? styles["active"] : styles["nonactive"]}
            onClick={() => setSignIn(true)}
          >
            Sign In
          </h2>
          <h2
            className={!signIn ? styles["active"] : styles["nonactive"]}
            onClick={() => setSignIn(false)}
          >
            Sign Up
          </h2>
        </div>
        <div className={styles.layer}></div>
        <p className={styles.point}>▲</p>
      </div>

      <div className={styles["wrapper-content"]}>
        {!signIn && (
          <div className={styles["sign-field"]}>
            <div className={styles["sign-field_input"]}>
              <p>First Name</p>
              <input type="text" placeholder="First Name" />
            </div>
            <div className={styles["sign-field_input"]}>
              <p>Last Name</p>
              <input type="text" placeholder="Last Name" />
            </div>
          </div>
        )}
        <div className={styles.input}>
          <p>Email</p>
          <input type="text" placeholder="Email" />
        </div>
        <div className={styles.input}>
          <p>Password</p>
          <input type="text" placeholder="Password" />
        </div>
        <div className={styles["mdl-textfield__wrap-text"]}>
          <p className={styles["forgot-text"]}>Forgot Password ?</p>
          <label
            className="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect"
            htmlFor="checkbox-1"
          >
            <input type="checkbox" id="checkbox-1" defaultChecked />
            <span className={styles["keep-text"]}>Keep me Signed In</span>
          </label>
        </div>
      </div>
      <button className={styles["sign-in-btn"]}>
        {signIn ? "Sign In" : "Sign Up"}
      </button>
    </div>
  );
};

export default Login;
