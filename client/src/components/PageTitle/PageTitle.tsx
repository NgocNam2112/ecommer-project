import React from "react";
import useStyles from "./styles";

interface IProps {
  page?: string[];
}

const PageTitle: React.FC<IProps> = ({ page }) => {
  const classes = useStyles();
  return (
    <>
      {page !== undefined && (
        <div className={classes["page-title"]}>
          {page.map((item, index: number) => {
            if (index !== page.length - 1) {
              return (
                <>
                  <p>{item}</p>
                  <span>/</span>
                </>
              );
            } else return <p>{item}</p>;
          })}
        </div>
      )}
    </>
  );
};

export default PageTitle;
