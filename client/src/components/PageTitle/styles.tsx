import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  "page-title": {
    height: 48,
    display: "flex",
    alignItems: "center",
    "& p": {
      fontFamily: "Open Sans",
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "12px",
      lineHeight: "16px",
      color: "#A9A9A9",
      "&:last-child": {
        color: "#151515",
      },
    },
    "& span": {
      fontSize: "12px",
      lineHeight: "16px",
      color: "#D1D1D1",
      margin: "0 8px",
    },
  },
});

export default useStyles;
