import React, { useState, useEffect } from "react";
import useStyles from "./styles";
import Rectangle from "../../images/HomPage/Rectangle";
import { Link } from "react-router-dom";

interface IProps {
  titleProduct: String;
  description: String;
  price: number;
  discount: number;
  stars?: JSX.Element[];
  image?: JSX.Element | string;
  isLoad?: boolean;
  key?: number;
  id?: string;
}

const ProductItem: React.FC<IProps> = ({
  titleProduct,
  description,
  price,
  discount,
  stars,
  image,
  isLoad,
  key,
  id,
}) => {
  const classes = useStyles();
  return (
    <div className={classes["item-div"]} key={key}>
      <div className={classes["blog-image"]}>
        {image}
        {isLoad ? null : <Rectangle />}
      </div>
      <h2>{titleProduct}</h2>
      <p>{description}</p>
      {stars}
      <div>
        <h2>{price} USD</h2>
        <Link to={`/detailsProduct/${id}`}>
          <button>Buy Now</button>
        </Link>
      </div>
      {discount !== 0 ? (
        <h4 className={classes["discount-percent"]}>-{discount}%</h4>
      ) : null}
    </div>
  );
};

export default ProductItem;
