import { Grid } from "@material-ui/core";
import DownWhiteArrow from "../../images/BodyFruit/DownWhiteArrow";
import useStyles from "./styles";
import { Link } from "react-router-dom";

interface IProps {
  productLength: number;
  productPerPage: number;
  currentNumber: number;
  pageNumber: Number;
}

const ShowMoreProduct: React.FC<IProps> = ({
  productLength,
  productPerPage,
  currentNumber,
  pageNumber,
}) => {
  const classes = useStyles();
  let pageNumbers = [];
  for (let i = 1; i <= Math.ceil(productLength / productPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <Grid item xs={12} className={classes["pagnition-page"]}>
      <div>
        <p>Page</p>
        {pageNumbers.map((number: number) => {
          return (
            <p>
              <Link
                to={`?page=${number}`}
                style={{
                  color: number === currentNumber ? "#6A983C" : "#151515",
                }}
              >
                {number}
              </Link>
            </p>
          );
        })}
      </div>
      {pageNumber === 1 ? (
        <button>
          Show more products <DownWhiteArrow />
        </button>
      ) : null}
      <div>
        <span>{productLength}</span>
        <span>Products</span>
      </div>
    </Grid>
  );
};

export default ShowMoreProduct;
