import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  "pagnition-page": {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 32,
    marginBottom: 64,
    padding: "16px 0",
    "& div": {
      "& span": {
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "600",
        fontSize: "12px",
        lineHeight: "18px",
        color: "#A9A9A9",
        marginLeft: 12,
        "&:first-child": {
          color: "#6A983C",
        },
      },
      "&:first-child": {
        display: "flex",
        "& p": {
          marginRight: 8,
          fontFamily: "Open Sans",
          fontStyle: "normal",
          fontWeight: "normal",
          fontSize: "12px",
          lineHeight: "16px",
          cursor: "pointer",
          "& a": {
            textDecoration: "none",
          },
        },
      },
    },
    "& button": {
      width: "214px",
      height: "47px",
      background: "#6A983C",
      border: "2px solid #46760A",
      boxSizing: "border-box",
      borderRadius: "12px",
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "15px",
      lineHeight: "22px",
      color: "#FFFFFF",
      outline: "none",
      cursor: "pointer",
    },
  },
  [theme.breakpoints.down(540)]: {
    "pagnition-page": {
      "& > button": {
        display: "none",
      },
    },
  },
}));

export default useStyles;
