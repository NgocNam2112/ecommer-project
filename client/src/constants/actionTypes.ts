export const CREATE = "CREATE";
export const UPDATE = "UPDATE";
export const DELETE = "DELETE";
export const FETCH_ALL = "FETCH_ALL";
export const FETCH_EACH_ITEM = "FETCH_EACH_ITEM";

//user route
export const REGISTER = "REGISTER";
export const LOGIN = "LOGIN";
export const FORGOT_PASSWORD = "FORGOT_PASSWORD";

//catagory route
export const GET_LIST_CATALOG = "GET_LIST_CATALOG";
