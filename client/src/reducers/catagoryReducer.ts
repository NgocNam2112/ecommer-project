import { GET_LIST_CATALOG } from "../constants/actionTypes";

const catalog: any = [];

const catagoryReducer = (state = catalog, action: any) => {
  switch (action.type) {
    case GET_LIST_CATALOG:
      return action.payload;
    default:
      return state;
  }
};
export default catagoryReducer;
