import { combineReducers } from "redux";

import productsReducer from "./productReducer";
import catagoryReducer from "./catagoryReducer";

export const reducer = combineReducers({
  product: productsReducer,
  catagory: catagoryReducer,
});
