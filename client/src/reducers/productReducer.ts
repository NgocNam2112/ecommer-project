import { IPayloadProduct } from "../api/types";
import { CREATE, DELETE, UPDATE, FETCH_ALL } from "../constants/actionTypes";

const products: Array<IPayloadProduct> = [];

const productsReducer = (state = products, action: any) => {
  switch (action.type) {
    case FETCH_ALL:
      return action.payload;
    default:
      return state;
  }
};
export default productsReducer;
