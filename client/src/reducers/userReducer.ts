import { REGISTER, LOGIN, FORGOT_PASSWORD } from "../constants/actionTypes";

const userReducer = (user = {}, action: any) => {
  switch (action.type) {
    case REGISTER:
      return action.payload;
    default:
      return { ...user };
  }
};

export default userReducer;
