import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  root: {
    padding: "0 45px",
  },
});

export default useStyles;
