const { json } = require("body-parser");
const User = require("../models/User");
const ErrorResponse = require("../utils/errorResponse");
const sendMail = require("../utils/sendMail");

module.exports = {
  registerAuth: async (req, res, next) => {
    const { firstName, lastName, email, password } = req.body;
    if (!firstName || !lastName || !email || !password) {
      return res.status(400).json("Please enter entire field");
    }
    try {
      const user = await User.create({ firstName, lastName, email, password });
      sendToken(user, 200, res);
    } catch (err) {
      return res
        .status(401)
        .json({ success: false, msg: "Invalid credentials" });
    }
  },
  loginAuth: async (req, res, next) => {
    try {
      const { email, password } = req.body;
      if (!email || !password) {
        return res
          .status(400)
          .json({ success: false, msg: "Please enter entire field" });
      }
      // Check that user exists by email
      const user = await User.findOne({ email }).select("+password");
      if (!user)
        return res
          .status(401)
          .json({ success: false, msg: "Invalid credentials" });

      // Check that password matchPassword
      const isMatch = await user.matchPassword(password);
      if (!isMatch) {
        return res
          .status(401)
          .json({ success: false, msg: "Invalid credentials" });
      }
      sendToken(user, 200, res);
    } catch (error) {
      res.status(401).json({ success: false, msg: err.message });
    }
  },
  forgotPassword: async (req, res, next) => {
    const { email } = req.body;
    const user = await User.findOne({ email });
    if (!user) {
      return res
        .status(400)
        .json({ success: false, msg: "No email cout not be sent" });
    }
    try {
      const resetToken = user.getResetPasswordToken();
      await user.save({
        validateBeforeSave: false,
      });
      const message = `<p>Verify code: ${resetToken}</p><p>Mã xác nhận tồn tại trong 10 phút</p>`;
      sendMail({
        to: email,
        subject: "Quên mật khẩu ?",
        message,
      });
      return res.status(201).json({ success: true, mgs: "Email sent" });
    } catch (error) {
      user.resetPasswordToken = undefined;
      user.resetPasswordExpire = undefined;
      return res.status(400).json({ success: false, mgs: error.message });
    }
  },
  resetPassword: async (req, res, next) => {
    const { resetCode } = req.params;
    const user = await User.findOne({
      resetPasswordToken: resetCode,
      resetPasswordExpire: { $gt: Date.now() },
    });
    if (!user) {
      return res
        .status(401)
        .json({ success: false, mgs: "No user to reset password" });
    }
    user.password = req.body.password;
    user.resetPasswordToken = undefined;
    user.resetPasswordExpire = undefined;
    await user.save();
    sendToken(user, 200, res);
  },
  updateCart: async (req, res, next) => {
    try {
      const { _id, productTitle, detailsIngredients, stars, price, discount } =
        req.body;
      const cart = {
        _id,
        productTitle,
        detailsIngredients,
        stars,
        price,
        discount,
      };
      const inCart = req.user.checkProduct(_id);

      return res.status(200).json({ success: true, msg: "Added to cart" });
    } catch (error) {
      return res.status(500).json({ success: false, msg: error.message });
    }
  },
};

const sendToken = (user, statusCode, res) => {
  const token = user.getSignedJwtToken();
  res.status(statusCode).json({ sucess: true, token });
};
