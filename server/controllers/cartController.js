const Cart = require("../models/Cart");

module.exports = {
  addToCart: (req, res) => {
    Cart.findOne({ user: req.user._id }).exec((error, cart) => {
      if (error) {
        return res.status(400).json({ error });
      }
      if (!cart) {
        // if cart not exist then create a new cart
        const cart = new Cart({
          user: req.user._id,
          cartItems: [{ ...req.body }],
        });
        cart.save((error, cart) => {
          if (error) {
            return res.status(400).json({ error });
          }
          if (cart) return res.status(201).json({ cart });
        });
      } else {
        // if cart exists then update cart by qyantity
        const product = req.body.product;
        const item = cart.cartItems.find((c) => c.product == product);
        if (item) {
          Cart.findOneAndUpdate(
            { user: req.user._id, "cartItems.product": product },
            {
              $set: {
                cartItems: {
                  ...req.body,
                  quantity: item.quantity + req.body.quantity,
                },
              },
            }
          ).exec((error, _cart) => {
            if (error) return res.status(400).json({ error });
            if (_cart) return res.status(201).json({ _cart });
          });
        } else {
          Cart.findOneAndUpdate(
            { user: req.user._id },
            {
              $push: {
                cartItems: req.body,
              },
            }
          ).exec((error, _cart) => {
            if (error) return res.status(400).json({ error });
            if (_cart) return res.status(201).json({ cart: _cart });
          });
        }
      }
    });
  },
  removeFromCart: (req, res) => {
    Cart.findOne({ user: req.user._id }).exec((error, cart) => {
      if (error) return res.status(400).json({ error });
      if (cart) {
        const { product } = req.body;
        const item = cart.cartItems.find((c) => c.product == product);
        if (item) {
          Cart.updateOne(
            { user: req.user._id },
            {
              $pull: {
                cartItems: { product: product },
              },
            }
          ).exec((error, result) => {
            if (error) return res.status(400).json({ error });
            if (result) return res.status(202).json({ result });
          });
        }
      }
    });
  },
  getProductInCart: (req, res) => {
    Cart.findOne({ user: req.user._id })
      .lean()
      .populate(
        "cartItems.product",
        "_id image productTitle image stars discount"
      )
      .exec((error, cart) => {
        if (error) {
          console.log(error);
          return res.status(400).json({ msg: error });
        }
        if (cart) {
          console.log(cart);
          return res.status(200).json({ cart });
        }
      });
  },
};
