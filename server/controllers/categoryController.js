const Category = require("../models/Category");
const mongoose = require("mongoose");

module.exports = {
  getCategory: async (req, res) => {
    try {
      const category = await Category.find();
      res.status(201).json(category);
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  },
  getCategoryById: async (req, res) => {
    const { id } = req.params;
    if (!mongoose.Types.ObjectId.isValid(id))
      return res.status(409).send("No category with this id");
    const category = await Category.findById(id);
    res.json(category);
  },
  addCategory: async (req, res) => {
    try {
      const { categoryName, categoryDescription } = req.body;
      const newCategory = new Category({ categoryName, categoryDescription });
      await Category.create(newCategory);
      res.status(201).json(newCategory);
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  },
  updateCategory: async (req, res) => {
    const { id } = req.params;
    const { categoryName, categoryDescription } = req.body;
    if (!mongoose.Types.ObjectId.isValid(id))
      return res.status(409).send(`No category valid with id: ${id}`);
    const updateCategory = { categoryName, categoryDescription, _id: id };
    await Category.findByIdAndUpdate(id, updateCategory, { new: true });
    res.json(updateCategory);
  },
  deleteCategory: async (req, res) => {
    const { id } = req.params;
    if (!mongoose.Types.ObjectId.isValid(id))
      return res.status(409).send(`No category valid id: ${id}`);
    await Category.findByIdAndRemove(id);
    res.send("Deleted successfully");
  },
};
