const Product = require("../models/Product");
const mongoose = require("mongoose");

module.exports = {
  getProducts: async (req, res) => {
    try {
      const products = await Product.find();
      res.status(200).json(products);
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  },
  getProductById: async (req, res) => {
    const { id } = req.params;
    console.log("id", id);
    if (!mongoose.Types.ObjectId.isValid(id))
      return res.status(409).send("There's no product with that id");
    const product = await Product.findById(id);
    res.json(product);
  },
  addProduction: async (req, res) => {
    const {
      image,
      relatedImage,
      productTitle,
      spaceforDescription,
      price,
      discount,
      categoryID,
      stars,
      detailsIngredients,
      detailDiscription,
      shipping,
      blogID,
      reviews,
      questions,
    } = req.body;
    const newProduct = {
      image,
      relatedImage,
      productTitle,
      spaceforDescription,
      price,
      discount,
      categoryID,
      stars,
      detailsIngredients,
      detailDiscription,
      shipping,
      blogID,
      reviews,
      questions,
    };
    try {
      await Product.create(newProduct);
      res.status(201).json(newProduct);
    } catch (error) {
      res.status(404).json({ message: error.message });
    }
  },
  deleteProduction: async (req, res) => {
    const { id } = req.params;
    if (!mongoose.Types.ObjectId.isValid(id))
      return res.status(409).send("There's no product with that id");

    await Product.findByIdAndDelete(id);
    res.json({ message: "Deleted successfully" });
  },
};
