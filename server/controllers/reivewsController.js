const Reviews = require("../models/Reviews");

module.exports = {
  // productId, content
  createNewReview: async (req, res) => {
    try {
      const { productId } = req.params;
      await Reviews.findOne({ productId }).exec((error, review) => {
        if (error) {
          console.log("error", error);
          return res.status(400).json({ error });
        }
        if (review) {
          Reviews.findOneAndUpdate(
            { productId: productId },
            {
              $push: {
                listReviews: [
                  {
                    user: req.user._id,
                    content: req.body.content,
                  },
                ],
              },
            }
          ).exec((error, _review) => {
            if (error) return res.status(400).json({ error });
            if (_review) return res.status(202).json({ _review });
          });
        } else {
          const review = new Reviews({
            productId: productId,
            listReviews: [
              {
                user: req.user._id,
                content: req.body.content,
              },
            ],
          });
          review.save((error, _review) => {
            if (error) return res.status(400).json({ error });
            if (_review) return res.status(202).json({ _review });
          });
        }
      });
    } catch (error) {
      return res.status(400).json({ success: false, error: error.message });
    }
  },
  //params: productId, body: {newContent, commentId}
  updateReview: async (req, res) => {
    const { productId } = req.params;
    const userId = req.user._id;
    console.log("productId", productId);
    console.log("userId", userId);
    try {
      await Reviews.updateOne(
        {
          $and: [
            {
              productId: productId,
            },
            {
              listReviews: {
                $elemMatch: {
                  user: userId,
                  _id: req.body._id,
                },
              },
            },
          ],
        },
        {
          $set: {
            "listReviews.$.content": req.body.content,
          },
        },
        (error, review) => {
          if (error) return res.status(400).json({ error });
          if (review) {
            console.log("review", review);
            return res
              .status(200)
              .json({ success: true, msg: "Update successfully" });
          }
        }
      );
    } catch (err) {
      return res.status(400).json({ success: false, msg: error.message });
    }
  },
  //params: productId, body: { userId, commentId}
  deleteReview: async (req, res) => {
    const { productId } = req.params;
    const userId = req.user._id;
    const { _id } = req.body;
    try {
      await Reviews.updateOne(
        {
          $and: [
            { productId: productId },
            { listReviews: { $elemMatch: { user: userId, _id: _id } } },
          ],
        },
        {
          $pull: {
            listReviews: { user: userId, _id: _id },
          },
        },
        (error, review) => {
          if (error) {
            console.log("error", error);
            return res.status(400).json({ error });
          }
          if (review) {
            return res.status(200).json({ review });
          } else {
            return res
              .status(400)
              .json({ success: false, msg: "No comment to delete" });
          }
        }
      );
    } catch (error) {
      return res.status(400).json({ success: false, msg: error.message });
    }
  },
  getListReviews: async (req, res) => {
    try {
      await Reviews.findOne({})
        .lean()
        .populate("listReviews.user", "_id firstName lastName")
        .exec((error, review) => {
          if (error) {
            return res.status(400).json({ error });
          }
          if (review) {
            return res.status(202).json({ review });
          }
        });
    } catch (error) {
      return res.status(400).json({ success: false, error: error.message });
    }
  },
};
