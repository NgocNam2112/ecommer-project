require("dotenv").config();
const express = require("express");
const cors = require("cors");
const connectDB = require("./config/database");
const categoryRoute = require("./routers/Category");
const productionsRoute = require("./routers/Productions");
const errorHandler = require("./middlewares/error");
connectDB();
const app = express();
const PORT = process.env.PORT || 5000;
app.use(express.json());
app.use(cors());

app.use("/category", categoryRoute);
app.use("/productions", productionsRoute);
app.use("/auth", require("./routers/Auth"));
app.use("/cart", require("./routers/Cart"));
app.use("/reviews", require("./routers/Reviews"));
app.use(errorHandler);

app.get("/", (req, res) => {
  res.send("Hello world");
});

const server = app.listen(PORT, () => console.log(`Server running on ${PORT}`));
process.on("unhandledRejection", (err, promise) => {
  console.log(`Logged Error: ${err.message}`);
  server.close(() => process.exit(1));
});
