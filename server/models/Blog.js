const mongoose = require("mongoose");

const BlogSchema = new mongoose.Schema(
  {
    image: String,
    tags: [String],
    productID: {
      type: mongoose.Schema.ObjectId,
      required: true,
      ref: "product",
    },
    categoryID: {
      type: mongoose.Schema.ObjectID,
      required: true,
      ref: "category",
    },
    productID: {
      type: mongoose.Schema.ObjectId,
      required: true,
      ref: "product",
    },
    userID: { type: mongoose.Schema.ObjectId, required: true, ref: "user" },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("blog", BlogSchema);
