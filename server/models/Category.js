const mongoose = require("mongoose");

const CategorySchema = new mongoose.Schema(
  {
    categoryName: {
      type: String,
      required: [true, "Please provide a category name"],
    },
    categoryDescription: {
      type: String,
      required: [true, "Please provide description for this category"],
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("category", CategorySchema);
