const mongoose = require("mongoose");

const ProductSchema = new mongoose.Schema(
  {
    image: {
      type: String,
      required: [true, "Please provide an image for this product"],
    },
    relatedImage: {
      img1: { type: String },
      img2: { type: String },
    },
    productTitle: {
      type: String,
      required: [true, "Please enter the title of product"],
    },
    spaceforDescription: {
      type: String,
      required: [true, "Please enter sapce for description here"],
    },
    price: Number,
    discount: Number,
    categoryID: {
      type: mongoose.Schema.ObjectId,
      ref: "category",
      required: [true, "This filed is required"],
    },
    stars: { type: Number, required: [true, "Please "] },
    detailsIngredients: {
      type: Object,
      required: [true, "Please enter ingredients detail for this product"],
    },
    detailDiscription: {
      type: Object,
      required: [true, "Please enter detail description for this product"],
    },
    shipping: { type: String, required: true },
    reviews: [String],
    questions: [String],
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("productions", ProductSchema);
