const mongoose = require("mongoose");

const ReviewSchema = new mongoose.Schema({
  productId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "productions",
    required: true,
  },
  listReviews: [
    {
      user: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
      content: String,
      time: { type: Date, default: Date.now },
    },
  ],
});

module.exports = mongoose.model("Reviews", ReviewSchema);
