const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const UserShema = new mongoose.Schema(
  {
    firstName: { type: String },
    lastName: { type: String },
    email: {
      type: String,
      required: [true, "Please enter your email"],
      unique: true,
      match: [
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        "Please provide a valid email",
      ],
    },
    password: {
      type: String,
      required: [true, "Please add a password"],
      minLength: 6,
      selected: false,
    },
    phonenumber: { type: String },
    address: String,
    townOrCity: String,
    stateOrCountry: String,
    postalCode: String,
    avatar: String,
    role: {
      type: Number,
      default: 0,
    },
    resetPasswordToken: String,
    resetPasswordExpire: Date,
  },
  {
    timestamps: true,
  }
);

UserShema.pre("save", async function (next) {
  if (!this.isModified("password")) {
    next();
  }
  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
  next();
});

UserShema.methods.matchPassword = async function (password) {
  return await bcrypt.compare(password, this.password);
};

UserShema.methods.getSignedJwtToken = function () {
  return jwt.sign({ id: this._id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRE,
  });
};

UserShema.methods.getResetPasswordToken = function () {
  const resetToken = Math.floor(Math.random() * 1000000);
  console.log("resetToken", resetToken);
  this.resetPasswordToken = resetToken;
  this.resetPasswordExpire = Date.now() + 10 * (60 * 1000);
  return resetToken;
};

UserShema.methods.checkProduct = function (_id) {
  return this.cart.indexOf(_id);
};
const User = mongoose.model("user", UserShema);

module.exports = User;
