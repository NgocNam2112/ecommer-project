const {
  registerAuth,
  loginAuth,
  forgotPassword,
  resetPassword,
  updateCart,
} = require("../controllers/authController");
const { protect } = require("../middlewares/protectAuth");
const router = require("express").Router();

router.post("/register", registerAuth);
router.post("/login", loginAuth);
router.post("/forgotPassword", forgotPassword);
router.put("/resetPassword/:resetCode", resetPassword);
router.patch("/updateCart", protect, updateCart);

module.exports = router;
