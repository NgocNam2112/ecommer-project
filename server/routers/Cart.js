const router = require("express").Router();
const { protect } = require("../middlewares/protectAuth");
const {
  addToCart,
  removeFromCart,
  getProductInCart,
} = require("../controllers/cartController");

router.post("/addToCart", protect, addToCart);
router.post("/removeFromCart", protect, removeFromCart);
router.get("/getCart", protect, getProductInCart);

module.exports = router;
