const express = require("express");
const {
  getProducts,
  getProductById,
  addProduction,
  deleteProduction,
} = require("../controllers/productionsController");

const router = express.Router();

router.get("/", getProducts);
router.post("/", addProduction);
router.get("/:id", getProductById);
router.delete("/:id", deleteProduction);

module.exports = router;
