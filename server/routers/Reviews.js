const router = require("express").Router();
const {
  createNewReview,
  updateReview,
  deleteReview,
  getListReviews,
} = require("../controllers/reivewsController");
const { protect } = require("../middlewares/protectAuth");

router.post("/createNewReivew/:productId", protect, createNewReview);
router.patch("/renewComment/:productId", protect, updateReview);
router.delete("/removeReview/:productId", protect, deleteReview);
router.get("/getComment", getListReviews);

module.exports = router;
